﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lipsch.VectorExtensions
{
    public static class VectorExtensions
    {
        public static Vector2[] ToVector2Array(this Vector3[] a)
        {
            Vector2[] vector2s = new Vector2[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                vector2s[i] = a[i];
            }

            return vector2s;
        }

        public static Vector3[] ToVector3Array(this Vector2[] a)
        {
            Vector3[] vector3s = new Vector3[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                vector3s[i] = a[i];
            }

            return vector3s;
        }

        public static Vector2 Rotate(this Vector2 v, float degrees)
        {
            degrees *= -1f;
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

            float tx = v.x;
            float ty = v.y;
            v.x = (cos * tx) - (sin * ty);
            v.y = (sin * tx) + (cos * ty);
            return v;
        }

        public static bool IsInRange(this Vector2 a, Vector2 b, float range)
        {
            return Vector2.Distance(a,b) <= range;
        }
    }

    public class VectorClockwiseComparer : IComparer<Vector2>
    {
        public Vector2 Position;

        public VectorClockwiseComparer(Vector2 pos)
        {
            Position = pos;
        }

        public int Compare(Vector2 a, Vector2 b)
        {
            float angleA = GetAngleOfLineBetweenTwoPoints(Position, a);
            float angleB = GetAngleOfLineBetweenTwoPoints(Position, b);

            if (angleA > angleB)
            {
                return -1;
            }
            else if (angleA == angleB)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public static float GetAngleOfLineBetweenTwoPoints(Vector2 p1, Vector2 p2)
        {
            float xDiff = p2.x - p1.x;
            float yDiff = p2.y - p1.y;
            return Mathf.Atan2(yDiff, xDiff) * Mathf.Rad2Deg;
        }
    }
}