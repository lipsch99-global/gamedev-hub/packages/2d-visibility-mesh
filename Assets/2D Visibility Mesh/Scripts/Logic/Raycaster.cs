﻿using Lipsch.VectorExtensions;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster
{
    public Vector2 Position { get; private set; }
    public Boundary[] Boundaries { get; private set; }

    private HashSet<Vector2> optimizedBoundaryPoints;

    public List<Vector2> crossPoints { get; private set; }

    public Raycaster(Vector2 position, Boundary[] boundaries)
    {
        Position = position;
        Boundaries = boundaries;

        if (Boundaries != null)
        {
            OptimizeBoundaries();

            GetOverlappingBoundaries();
        }
    }

    public void SetPosition(Vector2 newPos)
    {
        Position = newPos;
    }

    public Vector2? GetHitPositionWithMultipleBoundaries(Vector2 direction, Boundary[] boundaries = null, bool hitEdges = true)
    {
        if (boundaries == null)
        {
            boundaries = Boundaries;
        }

        Vector2? clostestPoint = null;
        float record = float.PositiveInfinity;

        for (int i = 0; i < boundaries.Length; i++)
        {
            Vector2? hit = GetHitPositionOnBoundary(boundaries[i], direction, hitEdges);

            if (hit != null)
            {
                float distance = Vector2.Distance(Position, hit.Value);
                if (distance < record)
                {
                    record = distance;
                    clostestPoint = hit.Value;
                }
            }
        }
        return clostestPoint;
    }

    public Vector2? GetHitPositionOnBoundary(Boundary b, Vector2 direction, bool hitEdges = true)
    {
        float x1 = b.A.x;
        float y1 = b.A.y;
        float x2 = b.B.x;
        float y2 = b.B.y;

        float x3 = Position.x;
        float y3 = Position.y;
        float x4 = Position.x + direction.x;
        float y4 = Position.y + direction.y;

        float den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        if (den == 0)
        {
            return null;
        }

        float t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
        float u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;

        Vector2 hitPoint = new Vector2(x1 + t * (x2 - x1), y1 + t * (y2 - y1));
        if (hitEdges)
        {
            if (t >= 0 && t <= 1 && u > 0)
            {
                return hitPoint;
            }
        }
        else
        {
            if (t > 0 && t < 1 && u > 0)
            {
                return hitPoint;
            }
        }
        return null;

    }

    public Vector2[] GetHitPoints()
    {
        List<Vector2> hitPoints = new List<Vector2>();
        foreach (var point in optimizedBoundaryPoints)
        {
            AddHits(point, ref hitPoints);
        }
        hitPoints.AddRange(crossPoints);
        return hitPoints.ToArray();
    }

    private void AddHits(Vector2 target, ref List<Vector2> hitPoints)
    {
        Vector2 hitDir = (target - Position).normalized;
        Vector2? hit = GetHitPositionWithMultipleBoundaries(hitDir);
        if (hit == null)
        {
            Debug.LogWarning("I don't think this should happen.");
        }
        else
        {
            hitPoints.Add(hit.Value);
            Vector2? hitRight = GetHitPositionWithMultipleBoundaries(hitDir.Rotate(0.0001f));
            if (hitRight != null)
            {
                if (Vector2.Distance(hitRight.Value, hit.Value) > 0.001f)
                {
                    hitPoints.Add(hitRight.Value);
                }
            }


            Vector2? hitLeft = GetHitPositionWithMultipleBoundaries(hitDir.Rotate(-0.0001f));
            if (hitLeft != null)
            {
                if (Vector2.Distance(hitLeft.Value, hit.Value) > 0.001f)
                {
                    hitPoints.Add(hitLeft.Value);
                }
            }
        }
    }

    public Vector2? GetCrossPointOfTwoLines(Boundary a, Boundary b)
    {
        float x1 = b.A.x;
        float y1 = b.A.y;
        float x2 = b.B.x;
        float y2 = b.B.y;

        float x3 = a.A.x;
        float y3 = a.A.y;
        float x4 = a.B.x;
        float y4 = a.B.y;

        float den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        if (den == 0)
        {
            return null;
        }

        float t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
        float u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;

        if ((t > 0 && t < 1) && (u > 0 && u < 1))
        {
            return new Vector2(x1 + t * (x2 - x1), y1 + t * (y2 - y1));
        }
        else
        {
            return null;
        }
    }


    private void OptimizeBoundaries()
    {
        optimizedBoundaryPoints = new HashSet<Vector2>();
        foreach (var bound in Boundaries)
        {
            optimizedBoundaryPoints.Add(bound.A);
            optimizedBoundaryPoints.Add(bound.B);
        }
    }

    public void GetOverlappingBoundaries()
    {
        crossPoints = new List<Vector2>();

        for (int a = 0; a < Boundaries.Length; a++)
        {
            for (int b = a + 1; b < Boundaries.Length; b++)
            {
                Vector2? crossPoint = GetCrossPointOfTwoLines(Boundaries[a], Boundaries[b]);

                if (crossPoint.HasValue)
                {
                    Vector2? crossPointRayHit = GetHitPositionWithMultipleBoundaries(crossPoint.Value - Position);

                    if (crossPointRayHit.Value.IsInRange(crossPoint.Value, 0.01f))
                    {
                        crossPoints.Add(crossPoint.Value);
                    }
                }
                else
                {
                    continue;
                }

            }
        }
    }
}

public struct Boundary
{
    public Vector2 A;
    public Vector2 B;

    public Boundary(Vector2 a, Vector2 b)
    {
        A = a;
        B = b;
    }
}
