﻿using Lipsch.VectorExtensions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeshBuilder : MonoBehaviour
{
    public PolygonCollider2D[] Boundaries;
    public MeshFilter MeshFilter;
    private Raycaster raycaster;

    private Vector2 casterPos;

    // Start is called before the first frame update
    private void Start()
    {
        CalculateBoundaries();
        BuildMesh();
    }

    private void CalculateBoundaries()
    {
        int boundsAmount = Boundaries.Sum(x => x.points.Length);

        Boundary[] bounds = new Boundary[boundsAmount];

        int i = 0;
        for (int b = 0; b < Boundaries.Length; b++)
        {
            Vector2 boundPosition = Boundaries[b].transform.position;
            for (int p = 0; p < Boundaries[b].points.Length; p++)
            {
                if (p == Boundaries[b].points.Length - 1)
                {
                    bounds[i] = new Boundary(Boundaries[b].transform.TransformPoint(Boundaries[b].points[p]),
                                            Boundaries[b].transform.TransformPoint(Boundaries[b].points[0]));
                }
                else
                {
                    bounds[i] = new Boundary(Boundaries[b].transform.TransformPoint(Boundaries[b].points[p]),
                                            Boundaries[b].transform.TransformPoint(Boundaries[b].points[p + 1]));
                }
                i++;
            }
        }

        raycaster = new Raycaster(casterPos, bounds);
    }
    private void BuildMesh()
    {
        Vector2[] points = raycaster.GetHitPoints();

        VectorClockwiseComparer vectorComparer = new VectorClockwiseComparer(casterPos);

        List<Vector2> orderedPoints = points.ToList();
        orderedPoints.Sort(vectorComparer);
        orderedPoints.Insert(0, casterPos);

        Mesh mesh = new Mesh();
        mesh.vertices = orderedPoints.ToArray().ToVector3Array();

        List<int> triangles = new List<int>();
        for (int i = 1; i < orderedPoints.Count; i++)
        {
            triangles.Add(0);
            triangles.Add(i);

            if (i == orderedPoints.Count - 1)
            {
                triangles.Add(1);
            }
            else
            {
                triangles.Add(i + 1);
            }
        }
        mesh.triangles = triangles.ToArray();

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        MeshFilter.mesh = mesh;
    }



    private void Rebuild()
    {
        CalculateBoundaries();
        BuildMesh();
    }
    // Update is called once per frame
    private void Update()
    {
        Rebuild();
    }
}
