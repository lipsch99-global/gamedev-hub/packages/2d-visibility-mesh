﻿using NUnit.Framework;
using System.Linq;
using UnityEngine;

namespace Tests
{
    public class RaycasterTests
    {
        [Test]
        public void SetPosition()
        {
            Raycaster raycaster = new Raycaster(Vector2.zero, null);

            raycaster.SetPosition(Vector2.up);

            Assert.AreEqual(raycaster.Position, Vector2.up);
        }

        #region GetHitPointOnBoundary

        [Test]
        public void GetHitPointOnBoundaryRightNoPoint()
        {
            Raycaster raycaster = new Raycaster(Vector2.zero, null);

            Boundary b = new Boundary(new Vector2(-1, 1), new Vector2(-1, -1));

            Vector2? hitPos = raycaster.GetHitPositionOnBoundary(b, Vector2.right);

            Assert.AreEqual(null, hitPos);
        }

        [Test]
        public void GetHitPointOnBoundaryDiagonal()
        {
            Raycaster raycaster = new Raycaster(Vector2.up, null);

            Boundary b = new Boundary(new Vector2(-1, 1), new Vector2(1, -1));

            Vector2? hitPos = raycaster.GetHitPositionOnBoundary(b, Vector2.left);

            Assert.AreEqual(new Vector2(-1, 1), hitPos);
        }

        [Test]
        public void GetHitPointOnBoundaryRightFindPoint()
        {
            Raycaster raycaster = new Raycaster(Vector2.zero, null);

            Boundary b = new Boundary(new Vector2(1, 1), new Vector2(1, -1));

            Vector2? hitPos = raycaster.GetHitPositionOnBoundary(b, Vector2.right);

            Assert.AreEqual(Vector2.right, hitPos);
        }

        [Test]
        public void GetHitPointOnBoundaryLeftNoPoint()
        {
            Raycaster raycaster = new Raycaster(Vector2.zero, null);

            Boundary b = new Boundary(new Vector2(1, 1), new Vector2(1, -1));

            Vector2? hitPos = raycaster.GetHitPositionOnBoundary(b, Vector2.left);

            Assert.AreEqual(null, hitPos);
        }

        [Test]
        public void GetHitPointOnBoundaryLeftFindPoint()
        {
            Raycaster raycaster = new Raycaster(Vector2.zero, null);

            Boundary b = new Boundary(new Vector2(-1, 1), new Vector2(-1, -1));

            Vector2? hitPos = raycaster.GetHitPositionOnBoundary(b, Vector2.left);

            Assert.AreEqual(Vector2.left, hitPos);
        }

        [Test]
        public void GetHitPointOnBoundaryHitTopEdge()
        {
            Raycaster raycaster = new Raycaster(Vector2.zero, null);

            Boundary b = new Boundary(new Vector2(1, 0), new Vector2(1, -1));

            Vector2? hitPos = raycaster.GetHitPositionOnBoundary(b, Vector2.right);

            Assert.AreEqual(Vector2.right, hitPos);
        }

        [Test]
        public void GetHitPointOnBoundaryHitBotEdge()
        {
            Raycaster raycaster = new Raycaster(Vector2.zero, null);

            Boundary b = new Boundary(new Vector2(1, 1), new Vector2(1, 0));

            Vector2? hitPos = raycaster.GetHitPositionOnBoundary(b, Vector2.right);

            Assert.AreEqual(Vector2.right, hitPos);
        }

        #endregion

        #region GetHitPoint

        [Test]
        public void GetHitPointFindClosePoint()
        {
            Boundary b1 = new Boundary(new Vector2(1, 1), new Vector2(1, -1));
            Boundary b2 = new Boundary(new Vector2(2, 1), new Vector2(2, -1));

            Raycaster raycaster = new Raycaster(Vector2.zero, new Boundary[] { b1, b2 });


            Vector2? hitPos = raycaster.GetHitPositionWithMultipleBoundaries(Vector2.right);

            Assert.AreEqual(Vector2.right, hitPos);
        }

        [Test]
        public void GetHitPointFindTwoWallsPoint()
        {
            Boundary b1 = new Boundary(new Vector2(1, 1), new Vector2(1, -1));
            Boundary b2 = new Boundary(new Vector2(-1, 1), new Vector2(-1, -1));

            Raycaster raycaster = new Raycaster(Vector2.zero, new Boundary[] { b1, b2 });


            Vector2? hitPos = raycaster.GetHitPositionWithMultipleBoundaries(Vector2.right);

            Assert.AreEqual(Vector2.right, hitPos);
        }

        [Test]
        public void GetHitPointFindSepcialPoint()
        {
            Boundary b1 = new Boundary(new Vector2(1, 1), new Vector2(2, 0.01f));
            Boundary b2 = new Boundary(new Vector2(1, -1), new Vector2(2, -0.01f));
            Boundary b3 = new Boundary(new Vector2(3, 1), new Vector2(3, -1));

            Raycaster raycaster = new Raycaster(Vector2.zero, new Boundary[] { b1, b2, b3 });


            Vector2? hitPos = raycaster.GetHitPositionWithMultipleBoundaries(Vector2.right);

            Assert.AreEqual(new Vector2(3, 0), hitPos);
        }

        #endregion

        #region GetHitPoints

        [Test]
        public void GetHitPointsCageWithOneWall()
        {
            Boundary b1 = new Boundary(new Vector2(-3, 3), new Vector2(3, 3)); //top
            Boundary b2 = new Boundary(new Vector2(3, 3), new Vector2(3, -3)); //right
            Boundary b3 = new Boundary(new Vector2(-3, -3), new Vector2(3, -3)); //bot
            Boundary b4 = new Boundary(new Vector2(-3, -3), new Vector2(-3, 3)); //left
            Boundary c1 = new Boundary(new Vector2(0, 2), new Vector2(2, 0));

            Raycaster raycaster = new Raycaster(Vector2.zero, new Boundary[] { b1, b2, b3, b4, c1 });


            Vector2[] hitPos = raycaster.GetHitPoints();

            Assert.IsTrue(ArrayContains(hitPos, c1.A)); //c top
            Assert.IsTrue(ArrayContains(hitPos, new Vector2(0, 3))); //boundary behind
            Assert.IsTrue(ArrayContains(hitPos, c1.B)); //c bot
            Assert.IsTrue(ArrayContains(hitPos, new Vector2(3, 0))); //boundary behind

            Assert.IsTrue(ArrayContains(hitPos, b1.A)); //top left bound

            Assert.IsFalse(ArrayContains(hitPos, b2.A)); //top right bound 
            Assert.IsTrue(ArrayContains(hitPos, b3.A)); //bot right bound
            Assert.IsTrue(ArrayContains(hitPos, b4.A)); //bot left bound

            Assert.AreEqual(8, hitPos.Length);
        }

        [Test]
        public void GetHitPointsOpenBoundsSpecial()
        {
            Boundary b1 = new Boundary(new Vector2(-4, 4), new Vector2(-1, 4)); //top left
            Boundary b2 = new Boundary(new Vector2(-1, 5), new Vector2(1, 5)); //top center
            Boundary b3 = new Boundary(new Vector2(1, 4), new Vector2(4, 4)); //top right
            Boundary b4 = new Boundary(new Vector2(4, 6), new Vector2(7, 3)); //top right diagonal
            Boundary b5 = new Boundary(new Vector2(5, 3), new Vector2(5, 0)); //right

            Raycaster raycaster = new Raycaster(Vector2.zero, new Boundary[] { b1, b2, b3, b4, b5 });


            Vector2[] hitPos = raycaster.GetHitPoints();

            Assert.IsTrue(ArrayContains(hitPos, b1.A));
            Assert.IsTrue(ArrayContains(hitPos, b1.B));
            Assert.IsTrue(ArrayContains(hitPos, b2.A));
            Assert.IsTrue(ArrayContains(hitPos, b2.B));
            Assert.IsTrue(ArrayContains(hitPos, b3.A));
            Assert.IsTrue(ArrayContains(hitPos, b3.B));
            Assert.IsTrue(ArrayContains(hitPos, new Vector2(5, 5)));
            Assert.IsTrue(ArrayContains(hitPos, new Vector2(6.25f, 3.75f)));
            Assert.IsTrue(ArrayContains(hitPos, b5.A));
            Assert.IsTrue(ArrayContains(hitPos, b5.B));

            Assert.AreEqual(12, hitPos.Length);
        }

        [Test]
        public void GetHitPointsConcave()
        {
            Boundary b1 = new Boundary(new Vector2(-3, 3), new Vector2(3, 3)); //top 
            Boundary b2 = new Boundary(new Vector2(3, 3), new Vector2(3, -3)); //right
            Boundary b3 = new Boundary(new Vector2(3, -3), new Vector2(-3, -3)); //bot
            Boundary b4 = new Boundary(new Vector2(-3, -3), new Vector2(-3, 3)); //left

            Boundary c1 = new Boundary(new Vector2(-2, 1), new Vector2(0, 1)); //top
            Boundary c2 = new Boundary(new Vector2(0, 1), new Vector2(-1, 0)); //top right
            Boundary c3 = new Boundary(new Vector2(-1, 0), new Vector2(0, -1)); //bot right
            Boundary c4 = new Boundary(new Vector2(0, -1), new Vector2(-2, -1)); //bot
            Boundary c5 = new Boundary(new Vector2(-2, -1), new Vector2(-2, 1)); //left

            Raycaster raycaster = new Raycaster(Vector2.right, new Boundary[] { b1, b2, b3, b4, c1, c2, c3, c4, c5 });


            Vector2[] hitPos = raycaster.GetHitPoints();

            Assert.IsFalse(ArrayContains(hitPos, b1.A));
            Assert.IsTrue(ArrayContains(hitPos, b1.B));
            Assert.IsTrue(ArrayContains(hitPos, b2.A));
            Assert.IsTrue(ArrayContains(hitPos, b2.B));
            Assert.IsTrue(ArrayContains(hitPos, b3.A));
            Assert.IsFalse(ArrayContains(hitPos, b3.B));

            Assert.IsFalse(hitPos.Any(x => x.x == -2 && (x.y >= -1 && x.y <= 1))); //should not get a point on c5

        }

        [Test]
        public void GetHitPointsConvex()
        {
            Boundary b1 = new Boundary(new Vector2(-3, 3), new Vector2(3, 3)); //top 
            Boundary b2 = new Boundary(new Vector2(3, 3), new Vector2(3, -3)); //right
            Boundary b3 = new Boundary(new Vector2(3, -3), new Vector2(-3, -3)); //bot
            Boundary b4 = new Boundary(new Vector2(-3, -3), new Vector2(-3, 3)); //left

            Boundary c1 = new Boundary(new Vector2(-2, 1), new Vector2(0, 0)); //top right
            Boundary c2 = new Boundary(new Vector2(0, 0), new Vector2(-2, -1)); //bot right
            Boundary c3 = new Boundary(new Vector2(-2, -1), new Vector2(-2, 1)); //left

            Raycaster raycaster = new Raycaster(Vector2.right, new Boundary[] { b1, b2, b3, b4, c1, c2, c3 });


            Vector2[] hitPos = raycaster.GetHitPoints();

            Assert.IsTrue(ArrayContains(hitPos, b1.A));
            Assert.IsTrue(ArrayContains(hitPos, b2.A));
            Assert.IsTrue(ArrayContains(hitPos, b3.A));
            Assert.IsTrue(ArrayContains(hitPos, b4.A));

            Assert.IsTrue(ArrayContains(hitPos, c1.A));
            Assert.IsTrue(ArrayContains(hitPos, c2.A));
            Assert.IsTrue(ArrayContains(hitPos, c3.A));

            Assert.IsFalse(hitPos.Any(x => x.x == -2 && (x.y >= -0.99f && x.y <= 0.99))); //should not get a point on c3
        }

        #endregion

        #region DoTwoLinesCross

        [Test]
        public void TwoLinesDoCross()
        {
            Boundary a = new Boundary(new Vector2(-1, 1), new Vector2(1, -1));
            Boundary b = new Boundary(new Vector2(1, 1), new Vector2(-1, -1));

            Raycaster r = new Raycaster(Vector2.zero, null);

            Assert.AreEqual(Vector2.zero, r.GetCrossPointOfTwoLines(a, b).Value);
        }
        [Test]
        public void TwoLinesDoNotCross()
        {
            Boundary a = new Boundary(new Vector2(-1, 1), new Vector2(1, -1));
            Boundary b = new Boundary(new Vector2(-2, 0), new Vector2(0, -2));

            Raycaster r = new Raycaster(Vector2.zero, null);

            Assert.AreEqual(null, r.GetCrossPointOfTwoLines(a, b));
        }

        #endregion

        #region GetOverlappingBoundaries

        //[Test]
        //public void OverlapBoundaries()
        //{
        //    Boundary a = new Boundary(new Vector2(-1, 1), new Vector2(1, -1));
        //    Boundary b = new Boundary(new Vector2(1, 1), new Vector2(-1, -1));

        //    Raycaster r = new Raycaster(Vector2.up, new Boundary[] { a, b });

        //    Boundary[] overlappingBoundaries = r.GetOverlappingBoundaries();

        //    Assert.AreEqual(2, overlappingBoundaries.Length);

        //    //B is the cross Point!
        //    Assert.AreEqual(-1, overlappingBoundaries[0].A.x);
        //    Assert.AreEqual(1, overlappingBoundaries[0].A.y);
        //    Assert.AreEqual(0, overlappingBoundaries[0].B.x);
        //    Assert.AreEqual(0, overlappingBoundaries[0].B.y);
        //    Assert.AreEqual(1, overlappingBoundaries[1].A.x);
        //    Assert.AreEqual(1, overlappingBoundaries[1].A.y);
        //    Assert.AreEqual(0, overlappingBoundaries[1].B.x);
        //    Assert.AreEqual(0, overlappingBoundaries[1].B.y);

        //}

        #endregion

        #region Helper Functions

        public static bool ArrayContains(Vector2[] haystack, Vector2 needle, float precision = 0.01f)
        {
            return haystack.Any(v =>
                (v.x <= needle.x + precision)
                && (v.x >= needle.x - precision)
                && (v.y <= needle.y + precision)
                && (v.y >= needle.y - precision)
            );
        }

        #endregion

    }
}
