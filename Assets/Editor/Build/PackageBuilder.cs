﻿using UnityEditor;

public static class PackageBuilder
{
    private const string PathToPackage = "Assets/2D Visibility Mesh/Scripts/Logic";
    private const string PackageName = "Builds/2D-Visibility-Mesh.unitypackage";

    public static void BuildPackage()
    {
        AssetDatabase.ExportPackage(PathToPackage, PackageName, ExportPackageOptions.IncludeDependencies | ExportPackageOptions.Recurse);
    }
}
