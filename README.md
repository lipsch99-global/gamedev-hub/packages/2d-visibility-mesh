# 2D Visibility Mesh

This package is used to create a 2D visibility or shadow effect like in the games; Nothing To Hide, Monaco or Gish.

## Dependencies
This project uses the [Vector Extensions](https://gitlab.com/gamedev-hub/packages/vector-extensions) as a dependency.

## Inputs
| Type | Done |
|------|------|
| Boundaries¹ | Yes |
| Polygon Collider | No |
| Box Collider | No |
| Circle Collider | No |
| Edge Collider | No |
| Capsule Collider | No |
| Composite Collider | No |
| Mesh Collider | No |
| Own Editing Tool | No |

¹ A class that provides a line in the format of two Vector2's.

## Outputs 
| Type | Done |
|------|------|
| Vertices | Yes |
| Mesh | Yes |
| Inverted Mesh(es) | No |

## Other Features
| Name | Description | Done |
|------|-------------|------|
| Static Inputs | Be able to mark certain inputs as static so their position does not get updated periodically | No |
| View Shader | A shader that applies to the camera so that you only see what the camera would see | No |

