#!/usr/bin/env bash

set -e
set -x

echo "Building for $BUILD_PATH"

mkdir -p $(pwd)/$BUILD_PATH

echo "LS:";
ls $(pwd) -a -R

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -projectPath $(pwd) \
  -quit \
  -batchmode \
  -executeMethod "PackageBuilder.BuildPackage" \
  -logFile /dev/stdout \

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

ls -la $(pwd)/$BUILD_PATH
[ -n "$(ls -A $(pwd)/$BUILD_PATH)" ] # fail job if build folder is empty